import PDFDocument from 'pdfkit'
import ordermodel from '../../models/ordermodel'
import {Request,Response} from 'express'

const invoice = (req:Request,res:Response)=>{

    /**
     * @param _id id of product to find
     */
    ordermodel.find({"_id":req.body._id})
    .populate('product_id')
    .populate('user_id')
    .then(result=>{
        res.status(200).json({success:'true',status:200,product:result})
    })
    .catch(err=>{
        res.status(404).json({success:'false',status:404,err:err.details});
    })
    
}
export default invoice