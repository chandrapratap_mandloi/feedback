import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedbacksService } from '../feedbacks.service';
import { getMatFormFieldDuplicatedHintError } from '@angular/material';
import * as moment from 'moment';
import { LoginService } from '../login.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public feedbacks = [];
  getFeed = JSON.parse(localStorage.getItem('userData'));
  array = ['#f68a8a', '#f3b36f', '#b3d9ff', '#f9f878', '#67d781', '#fffe88' , '#5d8ae9'];
  // public a=this.getFeed.Feedback.length != 0 ? this.getFeed.Feedback[1].f_d : '';
  public imga = ' http://92d99cf4.ngrok.io/' + this.getFeed.Image;
  constructor(
    private spinner: NgxSpinnerService,
    public logg: LoginService,
    private act: ActivatedRoute,
    private route: Router,
    private feedback: FeedbacksService
  ) {
  }
  // public date=moment(this.a).format('dddd, MMMM Do YYYY, h:mm:ss a');
  // newDate=moment(this.date, 'YYYYMMDD').fromNow();
  todayDate = moment().format('MMMM Do YYYY, h:mm:ss a');
  ngOnInit() {
    // debugger;
    // this.spinner.show();
    setTimeout(() => {
    }, 2000);
    if (!localStorage.getItem('firstLoad')) {
      localStorage[ 'firstLoad' ] = true;
      window.location.reload();
  } else {
      localStorage.removeItem('firstLoad');
  }
    this.logg.getfeeback().subscribe(item => {
      if (item.success === 'false') {
        alert(item.Message);
        this.spinner.hide();
      } else {
        this.feedbacks = item;
        console.log('this.feedbacks', this.feedbacks);
        console.log(item.Message);
        this.feedbacks = item;
        console.log('this.feedbacks', this.feedbacks);
        console.log(item.Message);
        this.spinner.hide();
      }
    });
    this.spinner.hide();
  }
  add() {
    // this.feedback.addFeedback().subscribe( (item)=>{
    //     // this.feedbacks= item;
    //     console.log(item)
    //   });

    this.route.navigate(['/addFeedbacks']);
    // console.log(this.log)
  }
  logout() {
    localStorage.clear();
    this.route.navigate(['/login']);
  }

  backColor() {
    const randomColor = this.array[
      Math.floor(Math.random() * this.array.length)
    ];
    return randomColor;
  }
}
