import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './user';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = ' http://92d99cf4.ngrok.io/';
  public neww: User = {
    name: '',
    email: '',
    attachment:
      'http://www.personalbrandingblog.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640-300x300.png' };
  constructor(private link: HttpClient) {}
  // public httpOptions = {
  //   headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  // };
  getUser(msg): Observable<any> {
    console.log(msg);
    // msg = {
    //   dev_email:'Ashutosh',
    //   dev_name:'Shubham'
    // }
    return this.link.post(this.baseUrl + 'users', msg);
    //  return this.link.get('http://993e2578.ngrok.io/dev');
  }
}
