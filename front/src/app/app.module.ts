import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import { LoginPanelComponent } from './login-panel/login-panel.component';
import {MatIconModule} from '@angular/material/icon';
import { DashboardComponent } from './dashboard/dashboard.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import { AddFeedbackComponent } from './add-feedback/add-feedback.component';
import { EmailValidDirective } from './email-valid.directive';
// import {MatInputModule} from '@angular/material/input';
import { HttpClientModule, HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserService } from './user.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { HttpErrorInterceptor } from './http-error.interceptor';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AuthGuard } from './auth.guard';
// import { DashGuard } from './dash.guard';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DateMomentPipe } from './date-moment.pipe';
import {MatDialogModule} from '@angular/material/dialog';
// import { TokenInterceptor } from './token.interceptors';
import { RandomNamePipe } from './random-name.pipe';
import { DatePipe } from './date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AdminPanelComponent,
    LoginPanelComponent,
    DashboardComponent,
    AddFeedbackComponent,
    EmailValidDirective,
    DateMomentPipe,
    RandomNamePipe,
    DatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatIconModule,
    MatToolbarModule,
    FormsModule,
    HttpClientModule,
    MatSnackBarModule,
    MatTooltipModule,
    NgxSpinnerModule,
    MatDialogModule
    // MatInputModule

  ],
  providers: [UserService,
    {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorInterceptor,
    multi: true
  },
  AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
