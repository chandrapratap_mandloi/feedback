import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'dateMoment'
})
export class DateMomentPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    // console.log('value=====>',value,'args====>',args);
    // console.log('console',moment(value).format('h:mm:ss '))
    // let a=moment(value).format('HH:mm:ss ');
    // console.log(a);
    // console.log(moment(value, 'HHmm').fromNow());
    // console.log(moment().format('DD-MM-YYYY'))
    const todayDate = moment().format('DD-MM-YYYY');
    const feedbackDate = moment(value).format('DD-MM-YYYY');
    if (todayDate === feedbackDate) {
      // console.log(moment().format())
      // console.log(moment(value, 'H').toNow());
      return moment(value, 'H').fromNow();
    } else {
      // console.log(moment(value, 'YYYYMMDD').fromNow());
      return moment(value, 'YYYYMMDD').fromNow();
    }
    // return moment(a).format('dddd, MMMM Do YYYY, h:mm:ss a');
  }
}
